import styled from 'styled-components';

const ValidationFeedbackVisibleWrapper = styled.div`
  width: 100%;
  height: 3em;
  vertical-align: middle;
  color: red;
  font-weight: bold;
  margin-top: 0.5em;
  color: #c72644;
`

const ValidationFeedbackHiddenWrapper = styled.div`
  width: 100%;
  height: 1em;
`


interface ValidationFeedbackProps {
  text: string,
  display: boolean
}

export const ValidationFeedback: React.FC<ValidationFeedbackProps> = (props) => {
  const { text, display } = props

  if (display) {
    return <ValidationFeedbackVisibleWrapper>
      {text}
    </ValidationFeedbackVisibleWrapper>
  }
  return <ValidationFeedbackHiddenWrapper>
  </ValidationFeedbackHiddenWrapper> 
}


