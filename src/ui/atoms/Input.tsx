import styled from 'styled-components';

export const Input = styled.input`
  background: none;
  border: none;
  border-bottom: 2px solid white;
  color: white;

  &:focus {
    outline: none;
  }
  &:disabled {
    color: #ddd;
    border-bottom-color: #ddd;
    cursor: pointer;
  }
`
