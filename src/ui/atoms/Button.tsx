import styled from 'styled-components';

export const Button = styled.button`
  background: none;
  border 2px solid white;
  color: white;
  padding: 0.5em;
  transition: 0.2s all ease;

  &:focus {
    outline: none;
    padding: 0.6em;
  }
  &:disabled {
    color: #ddd;
    border-color: #ddd;
    cursor: pointer;
  }
`
