import { Link, Redirect } from 'react-router-dom';
import styled from 'styled-components';

const WelcomeWrapper = styled.div`
  text-align: center;
  color: white;
  font-weight: bold;

  & a {
    color: white;
  }
`;

export const Welcome: React.FC = () => {
  const loggedInUser = localStorage.getItem("useremail");

  if (!loggedInUser) {
    return <Redirect to={'/login'} />
  }

  return <WelcomeWrapper>
    Welcome user!
    {' '}
    <Link to="/logout">Log out!</Link>
  </WelcomeWrapper>
}
