import styled from 'styled-components';

export const AppMain = styled.main`
  display: block;
  min-width: 360px;
  padding: 40px 0;
`
