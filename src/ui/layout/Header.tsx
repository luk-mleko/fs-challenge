import styled from 'styled-components';

const HeaderWrapper = styled.div`
  padding: 1em;
  background: #fafafa;
  color: #a3a3a3;
  text-align: center;
`

export const Header: React.FC = () => {
  return <HeaderWrapper>
    <h1>FS challenge</h1> 
  </HeaderWrapper>
}


