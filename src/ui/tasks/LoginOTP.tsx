import React, { useState } from 'react';
import { Button } from '../atoms/Button';
import { Input } from '../atoms/Input';
import { Label } from '../atoms/Label';
import { ValidationFeedback } from '../atoms/ValidationFeedback';


export interface LoginOTPRequest {
  email: string
  otpPassword: string
}

export type HandleOTPRequest = (reg: LoginOTPRequest) => void

interface LoginOTPProps {
  handleLoginRequest: HandleOTPRequest
  loading: boolean,
  email: string
}

export const LoginOTP: React.FC<LoginOTPProps> = (props) => {
  const { handleLoginRequest, loading, email } = props;

  const [password, setPassword] = useState("")
  const [validPassword, setValidPassword] = useState<boolean | undefined>()

  const passwordChange = (newValue: string) => {
    setPassword(newValue)
    validatePassword(newValue)
  }

  const validatePassword = (password: string) => {
    setValidPassword(password.length > 0)
  }

  const submitIfEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter" && validPassword) {
      handleSubmit()
    }
  }

  const handleSubmit = () => {
    handleLoginRequest({ email, otpPassword: password })
  }

  return <>
    <h2>Two step verification</h2>

    <p>Please provide a code we've sent to your inbox</p>

    <Label>Code</Label> 
    <Input
      type="password"
      value={password}
      onChange={(event) => { passwordChange(event.target.value) }}
      onKeyUp={submitIfEnter}
      tabIndex={4}
      disabled={loading}
    />
    <ValidationFeedback text="Your code should not be empty" display={validPassword === false} />

    <Button
      onClick={handleSubmit}
      disabled={!validPassword || loading}
      tabIndex={5}
    >
      Submit
    </Button>
  </>
}
