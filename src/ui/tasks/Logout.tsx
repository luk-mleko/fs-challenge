import { Redirect } from "react-router-dom"

export const Logout: React.FC = () => {
  localStorage.removeItem('useremail')
  return <Redirect to={'/'} />
}