import React, { useState } from 'react';
import { Button } from '../atoms/Button';
import { Input } from '../atoms/Input';
import { Label } from '../atoms/Label';
import { ValidationFeedback } from '../atoms/ValidationFeedback';

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export interface LoginEmailRequest {
  email: string
  password: string
}

export type HandleLoginRequest = (reg: LoginEmailRequest) => void

interface LoginEmailProps {
  handleLoginRequest: HandleLoginRequest
  loading: boolean
}

export const LoginEmail: React.FC<LoginEmailProps> = (props) => {
  const { handleLoginRequest, loading } = props;

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [validEmail, setValidEmail] = useState<boolean | undefined>()
  const [validPassword, setValidPassword] = useState<boolean | undefined>()

  const emailChange = (newValue: string) => {
    setEmail(newValue)
  }

  const passwordChange = (newValue: string) => {
    setPassword(newValue)
    validatePassword(newValue)
  }

  const validateEmail = () => {
    setValidEmail(emailRegex.test(email.toLowerCase()));
  }

  const validatePassword = (password: string) => {
    setValidPassword(password.length > 0)
  }

  const submitIfEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter" && validForm) {
      handleSubmit()
    }
  }

  const handleSubmit = () => {
    handleLoginRequest({ email, password })
  }

  const validForm = validEmail && validPassword;

  return <>
    <h2>Login</h2>
    <Label>Email</Label>
    <Input
      type="email"
      value={email}
      onChange={(event) => { emailChange(event.target.value) }}
      onBlur={() => { validateEmail() }}
      tabIndex={1}
      disabled={loading}
    />
    <ValidationFeedback text="Please use correct email format: name@domain.com" display={validEmail === false} />

    <Label>Password</Label>
    <Input
      type="password"
      value={password}
      onChange={(event) => { passwordChange(event.target.value) }}
      onKeyUp={submitIfEnter}
      tabIndex={2}
      disabled={loading}
    />
    <ValidationFeedback text="Your password should not be empty" display={validPassword === false} />

    <Button
      onClick={() => { if (validForm) handleSubmit() }}
      disabled={!validForm || loading}
      tabIndex={3}
    >
      Submit
    </Button>
  </>
}
