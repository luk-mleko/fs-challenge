import axios from 'axios';
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import styled from 'styled-components'
import { Center } from '../atoms/Center';
import { ValidationFeedback } from '../atoms/ValidationFeedback';
import { HandleLoginRequest, LoginEmail } from './LoginEmail';
import { HandleOTPRequest, LoginOTP } from './LoginOTP';

const LoginWrapper = styled.div`
  border: 3px solid white;
  width: 300px;
  max-width: 90%;
  margin: 3em auto;
  padding: 10px;

  text-align: left;
  color: white;
`

interface LoginResponse {
  valid: boolean
  otp_required?: boolean
}

type LoginStatus = 'PENDING' | 'LOADING' | 'OTPPENDING' | 'OTPLOADING' | 'SUCCESS';
type ErrorType = 'CONNECTION' | 'WRONGUSER' | 'WRONGOTP' | null;

export const Login: React.FC = () => {
  const [status, setStatus] = useState<LoginStatus>('PENDING')
  const [serverError, setServerError] = useState<ErrorType>(null)
  const [email, setEmail] = useState<string | undefined>()
  const [redirect, setRedirect] = useState<true | undefined>()

  const handleEmailSubmit: HandleLoginRequest = async (loginRequestData) => {
    try {
      setStatus('LOADING')
      setServerError(null);
      setEmail(undefined);

      const response = await axios.post<LoginResponse>(
        "http://127.0.0.1:8000/users/",
        loginRequestData
      );
      
      if (response.data.valid && response.data.otp_required) {
        setEmail(loginRequestData.email);
        setStatus('OTPPENDING')
      } else if (response.data.valid) {
        setStatus('SUCCESS')
        setEmail(loginRequestData.email);
        loginUser(loginRequestData.email)
      } else {
        setStatus('PENDING')
        setServerError('WRONGUSER')
      }
    } catch (e) {
      console.error(e)
      setStatus('PENDING')
      setServerError('CONNECTION')
    }
  };

  const handleOTPSubmit: HandleOTPRequest = async (loginRequestData) => {
    try {
      setStatus('OTPLOADING')
      setServerError(null);

      const isValidOTP = await fakeOTPCall(loginRequestData.otpPassword)

      if (isValidOTP) {
        setStatus('SUCCESS')
        loginUser(loginRequestData.email)
      } else {
        setStatus('OTPPENDING')
        setServerError('WRONGOTP')
      }
    } catch (e) {
      console.error(e)
      setStatus('PENDING')
      setServerError('CONNECTION');
    }
  };

  const fakeOTPCall = async (password: string) => {
    await sleep(1000)
    return password === '111111'
  }

  function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  const loginUser = async (email: string) => {
    await sleep(1000)
    localStorage.setItem('useremail', email)
    setRedirect(true)
  }

  let errorMessage = ''
  switch (serverError) {
    case 'CONNECTION': 
      errorMessage = 'Error communicating with server'
      break
    case 'WRONGUSER':
      errorMessage = 'Email or password do not match'
      break
    case 'WRONGOTP':
      errorMessage = 'Wrong code!'
      break
  }

  if (redirect) {
    return <Redirect to={'/'} />
  }

  return <LoginWrapper>
    {(status === 'PENDING' || status === 'LOADING') && <LoginEmail handleLoginRequest={handleEmailSubmit} loading={status === 'LOADING'}/> }

    {email && (status === 'OTPPENDING' || status === 'OTPLOADING') && <LoginOTP handleLoginRequest={handleOTPSubmit} loading={status === 'OTPLOADING'} email={email}/>}

    {status === 'SUCCESS' && <Center>You're logged in 😀</Center>}

    <ValidationFeedback text={errorMessage} display={serverError !== null}/>

  </LoginWrapper>
}
