import styled from 'styled-components'
import { BrowserRouter } from 'react-router-dom';
import { Main } from './Main';

const AppWrapper = styled.div`
  background-color: #a3a3a3;
  width: 100%;
  height: 100vh;
`

function App() {
  return (
    <AppWrapper>
      <BrowserRouter>
        <Main />
      </BrowserRouter>
    </AppWrapper>
  );
}

export default App;
