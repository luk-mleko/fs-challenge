import { Route } from 'react-router-dom';
import { Welcome } from './ui/app/Welcome';
import { Header } from './ui/layout/Header';
import { Login } from './ui/tasks/Login';
import { AppMain } from './ui/app/AppMain';
import { Logout } from './ui/tasks/Logout';




export const Main: React.FC = () => {
  return (
    <div className="App">
      <Header />
      <AppMain>
        <Route exact path="/login" component={Login} />
        <Route exact path="/logout" component={Logout} />
        <Route exact path="*" component={Welcome} />
      </AppMain>
    </div>
  )
}
