from enum import Enum
from typing import Optional
from fastapi import FastAPI

from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    "http://localhost:3000",
    "http://127.0.0.1:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class LoginRequest(BaseModel):
  email: str
  password: str

class LoginResponse(BaseModel):
  valid: bool
  otp_required: bool

@app.post("/users/")
async def login_user(req: LoginRequest):
  if req.email == 'name@domain.com' and req.password == 'pass':
    return {
      'valid': True,
      'otp_required': True
    }
  elif req.email == 'othername@domain.com' and req.password == 'password':
    return {
      'valid': True,
      'otp_required': False
    }
  else:
    return {
      'valid': False
    }
