# Coding challenge for FS

## Installation

#### Frontend
- run `yarn install` in the main folder

#### Backend (API)
- run `pip install fastapi[all]` (or `pip3 install fastapi[all]` if you're using separate naming for python 3)


### Runing the application
- run `yarn start` in the main folder to start up the frontend application
- open second terminal, go to `./api` folder and run `uvicorn main:app --reload`

### Available users

- user: `name@domain.com` with password: `pass` and OTP: `111111`
- user: `othername@domain.com` with password: `password` and no OTP


## Implemented stories

- [x] Login form with email and password fields and submit button
- [x] User should be able to submit the form using "Sign in" button or by hitting Enter key on the keyboard
- [x] Form submission should be available only when the form is filled and the user provides valid email (valid in terms of format, not existing user account)
- [x] Validation errors should be shown on the screen
- [x] Login failure should be presented to the user as a snackbar/notification
- [x] After successful email / password authentication the user should provide OTP/TOTP code for two-factor authentication if backend decides
- [x] After successful two-step authentication the user should be logged in and redirected to home page
- [x] The user should remain logged in when the app is reloaded
